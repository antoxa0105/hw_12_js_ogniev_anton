// 1. setTimeout определяет время до запуска функции и выполняется один раз, setInterval будет выполнять функцию с определенным интервалом;
// 2. Если задать нулевую задержку, то вначале выполнятся все блокирующие функции (к примеру Alert), а затем сработает timeOut;
// 3. Если не вызвать clearInterval, то setInterval будет выполняться бесконечно.

showPictures();
let currentImg = 0;  
let previousImg;

const arr = Array.from(document.getElementsByClassName("image-to-show"));
function showPictures () {
interval = setInterval(() => {  
    previousImg = currentImg;
    if (currentImg === Object.keys(arr).length -1) {
        currentImg = 0;
      }  else {currentImg++};           
    arr[currentImg].classList.remove('hidden-img');
    if (currentImg === 0) {
        arr[previousImg].classList.add('hidden-img');
    } else {arr[currentImg - 1].classList.add('hidden-img')}     
}, 2000);
};

document.querySelector('.button-wrapper').addEventListener('click', function (event){
    let btnStop = event.target.closest('.btnStop');
    let btnStart = event.target.closest('.btnStart');
    if (btnStop){clearInterval(interval);
    btnStop.setAttribute("disabled", "disabled");
    document.querySelector('.btnStart').removeAttribute("disabled")};
    if (btnStart){clearInterval(interval),showPictures(interval)
      btnStart.setAttribute("disabled", "disabled");
      document.querySelector('.btnStop').removeAttribute("disabled");
    };
  });

